package com.wheatek.wtklib.net.callback;


import java.io.IOException;

public interface ICallBack<T> {

    void onNext(String responseBody) throws IOException;
    
    void onSuccess(T result, String code, String msg);

    void onNotLogin();
}
