package com.wheatek.wtklib.net.exception

import java.lang.Exception

class RequestException(val msg: String) : Exception(msg) {
}