package com.wheatek.wtklib.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;
import com.wheatek.wtklib.R;
import com.wheatek.wtklib.base.iInit.CreateInit;
import com.wheatek.wtklib.utils.ActivityQueueManager;
import com.wheatek.wtklib.widget.LoadingDialog;

/**
 * Created by shiju.wang on 2018/2/10.
 */

public abstract class BaseActivity<T extends ViewBinding> extends AppCompatActivity implements CreateInit<T> {

    // 子类view容器
    private FrameLayout layout_content;

    private TextView tvTitle;

    private TextView tvBack;

    protected Context context;

    protected T mBinding;

    private LoadingDialog mLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (setTransparentStatusBar()) {
            transparentStatusBar();
        }
        super.setContentView(R.layout.activity_base);
        ActivityQueueManager.getInstance().pushActivity(this);
        context = this;
        init();
    }

    private void init() {
        layout_content = super.findViewById(R.id.frameLayout);
        tvTitle = super.findViewById(R.id.tvTitle);
        tvBack = super.findViewById(R.id.tvBack);

        mBinding = bindView();
        setContentView(mBinding.getRoot());

        // 初始化数据
        prepareData(getIntent());
        // 初始化view
        initView();
        // 初始化事件
        initEvent();
        // 加载数据
        initData();

        tvBack.setOnClickListener(v -> finish());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // 初始化数据
        prepareData(intent);
    }

    // 设置标题
    public void setTitle(String title) {
        tvTitle.setText(title);
    }

    public void hideBackButton() {
        tvBack.setVisibility(View.GONE);
    }

    /**
     * @param view
     */
    public void setContentView(View view) {
        layout_content.removeAllViews();
        layout_content.addView(view);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityQueueManager.getInstance().popActivity(this);
    }

    /**
     * 设置optionMenu
     *
     * @return
     */
    protected int setOptionMenu() {
        return 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        int res = setOptionMenu();
        if (res != 0) {
            getMenuInflater().inflate(res, menu);
            return true;
        } else {
            return super.onCreateOptionsMenu(menu);
        }
    }

    protected void showLoading(boolean show) {
        showLoading(show, "");
    }

    protected void showLoading(boolean show, String tip) {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
            mLoadingDialog.setMessage("正在请求...");
        }
        if (show) {
            if (!tip.isEmpty()) {
                mLoadingDialog.setMessage(tip);
            }
            mLoadingDialog.show();
        } else {
            mLoadingDialog.dismiss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * [是否设置沉浸状态栏]
     */
    public boolean setTransparentStatusBar() {
        return false;
    }

    /**
     * 状态栏透明
     */
    private void transparentStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
