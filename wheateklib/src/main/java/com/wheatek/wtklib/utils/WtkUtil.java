package com.wheatek.wtklib.utils;

import android.os.Build;

import java.lang.reflect.Method;

public class WtkUtil {

    /**
     * 或者设备型号
     *
     * @return
     */
    public static String getModel() {
        return Build.MODEL;
    }

    /**
     * 获取序列号
     *
     * @return
     */
    public static String getSerialNo() {
        String sn = "";
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {// 8.0+
//            sn = Build.SERIAL;
//        } else {
        sn = getAndroidOsSystemProperties("ro.serialno");
//        }
        if (sn.equals("unknown")) {
            sn = "SHEBEI001001001";
        }
        return sn;
    }

    private static String getAndroidOsSystemProperties(String key) {
        String ret;
        try {
            Method getMethod = Class.forName("android.os.SystemProperties").getMethod("get", String.class);
            if ((ret = (String) getMethod.invoke(null, key)) != null) {
                return ret;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
}
